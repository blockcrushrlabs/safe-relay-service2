import logging
from typing import Union
from datetime import datetime

from django_eth.constants import (NULL_ADDRESS, SIGNATURE_S_MAX_VALUE,
                                  SIGNATURE_S_MIN_VALUE)
from django_eth.serializers import (EthereumAddressField, Sha3HashField,
                                    TransactionResponseSerializer)
from gnosis.safe.ethereum_service import EthereumService
from gnosis.safe.serializers import (SafeMultisigEstimateTxSerializer,
                                     SafeMultisigTxSerializer,
                                     SafeMultisigSubTxSerializer,
                                     SafeSignatureSerializer)
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from . import models

from .relay_service import RelayServiceProvider

# Ethereum addresses have 40 chars (without 0x)
ADDRESS_LENGTH = 40

logger = logging.getLogger(__name__)


class BaseEventSerializer(serializers.BaseSerializer):
    class Meta:
        abstract = True

    def __init__(self, *args, **kwargs):
        """
        Gets the kwargs passed to the serializer and produces a new data dictionary with:
            address: string
            creation_date_time: datetime
            creation_block: int
        In addition, all parameters contained in kwargs['data']['params'] are re-elaborated and added to
        the final data dictionary
        """
        if kwargs.get('block'):
            self.block = kwargs.pop('block')

        super().__init__(*args, **kwargs)

        self.initial_data = self.parse_event_data(kwargs.pop('data'))

    def parse_event_data(self, event_data):
        """
        Extract event_data and move event params moved to root object
        :param event_data: dictionary
        :return: dictionary with event params
        """

        return {param['name']: param['value'] for param in event_data.get('params')}


class ContractSerializer(BaseEventSerializer):
    """
    Serializes a Contract entity
    """

    class Meta:
        fields = ('address',)

    address = serializers.CharField(max_length=ADDRESS_LENGTH)

    def parse_event_data(self, event_data):
        """
        Move event params moved to root object
        :return: dictionary with event params moved to root object
        """

        parsed_event_data = super().parse_event_data(event_data)
        parsed_event_data.update({
            'address': event_data.get('address'),
        })
        return parsed_event_data


class BlockTimestampedSerializer(BaseEventSerializer):
    """
    Serializes the block information
    """

    class Meta:
        fields = ('creation_date_time', 'creation_block',)

    creation_date_time = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S")
    creation_block = serializers.IntegerField()

    def parse_event_data(self, event_data):
        parsed_event_data = super().parse_event_data(event_data)
        parsed_event_data.update({
            'creation_date_time': datetime.fromtimestamp(self.block.get('timestamp')),
            'creation_block': self.block.get('number'),
        })
        return parsed_event_data


# custom serializers for events


class StatusChangedEventSerializer(BlockTimestampedSerializer, serializers.ModelSerializer):
    """
    Serializes a SubscriptionStatusChange event
    """

    class Meta:
        model = models.SubscriptionStatusChangedEvents
        fields = BlockTimestampedSerializer.Meta.fields + ('hash', 'prev', 'next')

    hash = serializers.CharField(max_length=80)
    prev = serializers.IntegerField(default=0)
    next = serializers.IntegerField(default=0)

    def parse_event_data(self, event_data):
        parsed_event_data = super().parse_event_data(event_data)
        parsed_event_data.update({
            'hash': parsed_event_data['hash'].hex()[2:],
            'prev': int(parsed_event_data['prev'].hex(), 16),
            'next': int(parsed_event_data['next'].hex(), 16)
        })
        return parsed_event_data

    def rollback(self):
        self.instance.delete()


class OraclizedDenominationEventSerializer(BlockTimestampedSerializer, serializers.ModelSerializer):
    """
    Serializes a SubscriptionOraclizedDenomination event
    """

    class Meta:
        model = models.SubscriptionOraclizedDenominationEvents
        fields = BlockTimestampedSerializer.Meta.fields + ('hash', 'dynPriceFormat', 'conversionRate', 'paymentTotal')

    hash = serializers.CharField(max_length=80)
    dynPriceFormat = serializers.CharField(max_length=80)
    conversionRate = serializers.CharField(max_length=80)
    paymentTotal = serializers.CharField(max_length=80)

    def parse_event_data(self, event_data):
        parsed_event_data = super().parse_event_data(event_data)
        parsed_event_data.update({
            'hash': parsed_event_data['hash'],
            'dynPriceFormat': int(parsed_event_data['dynPriceFormat'].hex(), 16),
            'conversionRate': str(parsed_event_data['conversionRate']),
            'paymentTotal': str(parsed_event_data['paymentTotal'])
        })
        return parsed_event_data

    def rollback(self):
        self.instance.delete()


class NextPaymentEventSerializer(BlockTimestampedSerializer, serializers.ModelSerializer):
    """
    Serializes a SubscriptionStatusChange event
    """

    class Meta:
        model = models.SubscriptionNextPaymentEvents
        fields = BlockTimestampedSerializer.Meta.fields + ('hash', 'nextWithdraw')

    hash = serializers.CharField(max_length=80)
    nextWithdraw = serializers.IntegerField(default=0)

    def parse_event_data(self, event_data):
        parsed_event_data = super().parse_event_data(event_data)
        parsed_event_data.update({
            'hash': parsed_event_data['hash'],
            'nextWithdraw': parsed_event_data['nextWithdraw']
        })
        return parsed_event_data

    def rollback(self):
        self.instance.delete()


# TODO Refactor
def validate_gas_token(address: Union[str, None]) -> str:
    """
    Raises ValidationError if gas token is not valid
    :param address: Gas Token address
    :return: address if everything goes well
    """
    if address and address != NULL_ADDRESS:
        try:
            token_db = models.Token.objects.get(address=address)
            if not token_db.gas:
                raise ValidationError('Token %s - %s cannot be used as gas token' % (token_db.name, address))
        except models.Token.DoesNotExist:
            raise ValidationError('Token %s not found' % address)
    return address


class SafeCreationSerializer(serializers.Serializer):
    s = serializers.IntegerField(min_value=SIGNATURE_S_MIN_VALUE,
                                 max_value=SIGNATURE_S_MAX_VALUE)
    owners = serializers.ListField(child=EthereumAddressField(), min_length=1)
    threshold = serializers.IntegerField(min_value=1)
    payment_token = EthereumAddressField(default=None, allow_null=True, allow_zero_address=True)
    wallet_type = serializers.CharField(default="customer")

    def validate_payment_token(self, value):
        return validate_gas_token(value)

    def validate(self, data):
        super().validate(data)
        owners = data['owners']
        threshold = data['threshold']

        if threshold > len(owners):
            raise ValidationError("Threshold cannot be greater than number of owners")

        return data


class SafeRelayMultisigEstimateTxSerializer(SafeMultisigEstimateTxSerializer):
    def validate_gas_token(self, value):
        return validate_gas_token(value)

    def validate(self, data):
        super().validate(data)
        return data


class SafeRelayMultisigTxSerializer(SafeMultisigTxSerializer):
    signatures = serializers.ListField(child=SafeSignatureSerializer())

    def validate(self, data):
        super().validate(data)

        safe_address = data['safe']
        signatures = data['signatures']

        relay_service = RelayServiceProvider()
        tx_hash = relay_service.get_hash_for_safe_tx(safe_address, data['to'], data['value'], data['data'],
                                                     data['operation'], data['safe_tx_gas'], data['base_gas'],
                                                     data['gas_price'], data['gas_token'], data['refund_receiver'],
                                                     data['nonce'])

        owners = [EthereumService.get_signing_address(tx_hash,
                                                      signature['v'],
                                                      signature['r'],
                                                      signature['s']) for signature in signatures]

        signature_pairs = [(s['v'], s['r'], s['s']) for s in signatures]
        if not relay_service.check_hash(tx_hash, relay_service.signatures_to_bytes(signature_pairs), owners):
            raise ValidationError('Signatures are not sorted by owner: %s' % owners)

        data['owners'] = owners
        return data


class SafeRelayMultisigSubTxExecuteSerializer(serializers.Serializer):
    execute_ids = serializers.ListField(min_length=1)

    def validate(self, data):
        super().validate(data)
        return data


class SafeRelayMultisigSubTxSerializer(SafeMultisigSubTxSerializer):
    signatures = serializers.ListField(child=SafeSignatureSerializer())

    def validate(self, data):
        super().validate(data)

        sub_module_address = data['sub_module_address']
        signatures = data['signatures']

        relay_service = RelayServiceProvider()
        eip1337_hash = relay_service.get_hash_for_eip_1337(
            sub_module_address,
            data['to'],
            data['value'],
            data['data'],
            data['period'],
            data['start_date'],
            data['end_date'],
            data['uniq_id']
        )

        owners = [
            EthereumService.get_signing_address(
                eip1337_hash,
                signature['v'],
                signature['r'],
                signature['s']
            ) for signature in signatures
        ]

        signature_pairs = [(s['v'], s['r'], s['s']) for s in signatures]
        if not relay_service.check_hash(eip1337_hash, relay_service.signatures_to_bytes(signature_pairs), owners):
            raise ValidationError('Signatures are not sorted by owner: %s' % owners)

        data['owners'] = owners
        data['hash'] = eip1337_hash
        return data


# ================================================ #
#                Responses                         #
# ================================================ #
class SignatureResponseSerializer(serializers.Serializer):
    """
    Use CharField because of JavaScript problems with big integers
    """
    v = serializers.CharField()
    r = serializers.CharField()
    s = serializers.CharField()


class SafeResponseSerializer(serializers.Serializer):
    address = EthereumAddressField()
    master_copy = EthereumAddressField()
    nonce = serializers.IntegerField(min_value=0)
    threshold = serializers.IntegerField(min_value=1)
    owners = serializers.ListField(child=EthereumAddressField(), min_length=1)


class TxListSerializer(serializers.Serializer):
    subscriptions = serializers.DictField()
    transactions = serializers.ListField()


class SafeLookupResponseSerializer(serializers.Serializer):
    safe = EthereumAddressField()
    subscription_module = EthereumAddressField()
    master_copy = EthereumAddressField()
    threshold = serializers.IntegerField(min_value=1)
    owners = serializers.ListField(child=EthereumAddressField(), min_length=1)


class SafeTransactionCreationResponseSerializer(serializers.Serializer):
    signature = SignatureResponseSerializer()
    tx = TransactionResponseSerializer()
    payment = serializers.CharField()
    payment_token = EthereumAddressField(allow_null=True, allow_zero_address=True)
    safe = EthereumAddressField()
    subscription_module = EthereumAddressField()
    deployer = EthereumAddressField()
    funder = EthereumAddressField()


class SafeFundingResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SafeFunding
        fields = ('safe_funded', 'deployer_funded', 'deployer_funded_tx_hash', 'safe_deployed', 'safe_deployed_tx_hash')


class SafeMultisigTxResponseSerializer(serializers.Serializer):
    transaction_hash = Sha3HashField()


class SafeMultisigSubTxExecuteResponseSerializer(serializers.Serializer):
    processed = serializers.ListField(required=True)
    skipped = serializers.ListField(required=False)


class SafeMultisigSubTxResponseSerializer(serializers.Serializer):
    sub_tx_id = serializers.IntegerField(min_value=0)
    transaction_hash = Sha3HashField(required=False, allow_null=True)


class SafeMultisigEstimateTxResponseSerializer(serializers.Serializer):
    safe_tx_gas = serializers.IntegerField(min_value=0)
    base_gas = serializers.IntegerField(min_value=0)
    operational_gas = serializers.IntegerField(min_value=0)
    gas_price = serializers.IntegerField(min_value=0)
    last_used_nonce = serializers.IntegerField(min_value=0, allow_null=True)
    gas_token = EthereumAddressField(allow_null=True, allow_zero_address=True)
