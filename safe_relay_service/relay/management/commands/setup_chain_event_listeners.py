from django.core.management.base import BaseCommand
from django_celery_beat.models import IntervalSchedule, PeriodicTask
from django_eth_events.models import Block, Daemon


class Command(BaseCommand):
    help = 'Setup Event Listeners for Contracts'

    def add_arguments(self, parser):
        parser.add_argument('--start-block-number', type=int, required=False)

    def handle(self, *args, start_block_number, **options):

        if start_block_number is not None:
            Block.objects.all().delete()

            daemon = Daemon.get_solo()
            daemon.block_number = start_block_number - 1
            daemon.save()

            self.stdout.write(self.style.SUCCESS('Restart processing at block {}'.format(start_block_number)))

        # auto-create celery task

        task_name = 'django_eth_events.tasks.event_listener'
        if PeriodicTask.objects.filter(task=task_name).count():
            self.stdout.write(self.style.SUCCESS('Task was already created'))
        else:
            interval, _ = IntervalSchedule.objects.get_or_create(every=5, period='seconds')
            PeriodicTask.objects.create(
                name='Event Listener',
                task=task_name,
                interval=interval
            )
            self.stdout.write(self.style.SUCCESS('Created Periodic Task for Event Listener every 5 seconds'))
