from typing import List

from django.core.exceptions import ObjectDoesNotExist
from django_eth_events.chainevents import AbstractAddressesGetter

from safe_relay_service.relay.models import (SafeContract)


class SubscriptionModuleAddressGetter(AbstractAddressesGetter):
    """
    Returns the addresses used by event listener in order to filter logs triggered by Contract Instances
    """

    class Meta:
        model = SafeContract

    def get_addresses(self) -> List[str]:
        """
        Returns list of ethereum addresses
        :return: [address]
        """
        return list(self.Meta.model.objects.values_list('subscription_module_address', flat=True))

    def __contains__(self, address):
        """
        Checks if sub_module_address is contained on the SafeContract collection
        :param address: ethereum address string
        :return: Boolean
        """
        try:
            self.Meta.model.objects.get(subscription_module_address=address)
            return True
        except ObjectDoesNotExist:
            return False
