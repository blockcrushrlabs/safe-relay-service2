from json import dumps

from celery.utils.log import get_task_logger
from django_eth_events.chainevents import AbstractEventReceiver
from django_eth_events.utils import JsonBytesEncoder

from safe_relay_service.relay.serializers import (OraclizedDenominationEventSerializer, StatusChangedEventSerializer,
                                                  NextPaymentEventSerializer)

logger = get_task_logger(__name__)


class SubscriptionSerializerEventReceiver(AbstractEventReceiver):
    class Meta:
        events = {
        }
        primary_key_name = 'hash'

    def save(self, decoded_event, block_info=None):
        print("save")
        event_name = decoded_event.get('name')
        if self.Meta.events.get(event_name):
            if block_info:
                serializer = self.Meta.events.get(event_name)(data=decoded_event, block=block_info)
            else:
                serializer = self.Meta.events.get(event_name)(data=decoded_event)
            # Only valid data goes forward, non valid data is logged

            if serializer.is_valid():
                instance = serializer.save()
                # logger.info('Event Receiver {} added: {}'.format(self.__class__.__name__,
                #                                                  dumps(decoded_event,
                #                                                        sort_keys=True,
                #                                                        indent=4,
                #                                                        cls=JsonBytesEncoder)))
                # serializer model instance is returned in order to django-eth-events know it was a valid event
                return instance
            else:
                # logger.warning('INVALID Data for Event Receiver {} save: {}'.format(self.__class__.__name__,
                #                                                                     dumps(decoded_event,
                #                                                                           sort_keys=True,
                #                                                                           indent=4,
                #                                                                           cls=JsonBytesEncoder)))
                logger.warning(serializer.errors)

    def rollback(self, decoded_event, block_info=None):
        event_name = decoded_event.get('name')
        print("rollback")
        serializer_class = self.Meta.events.get(event_name)
        # Get primary key name from Meta.primary_key_name, it can be the same for all Events (string) or different for
        # each one (dictionary)
        if type(self.Meta.primary_key_name) is str:
            primary_key_name = self.Meta.primary_key_name
        else:
            primary_key_name = self.Meta.primary_key_name.get(event_name)
        primary_key = next(filter(lambda x: x.get('name') == primary_key_name,
                                  decoded_event.get('params'))).get('value')

        # Find instance to update/delete
        instance = serializer_class.Meta.model.objects.get(
            hash=primary_key
        )

        serializer = serializer_class(instance, data=decoded_event, block=block_info)
        if serializer.is_valid():
            serializer.rollback()
            logger.info('Event Receiver {} reverted: {}'.format(self.__class__.__name__, dumps(decoded_event,
                                                                                               sort_keys=True,
                                                                                               indent=4,
                                                                                               cls=JsonBytesEncoder)))
        else:
            logger.warning('INVALID Data for Event Receiver {} rollback: {}'.format(self.__class__.__name__,
                                                                                    dumps(decoded_event,
                                                                                          sort_keys=True,
                                                                                          indent=4,
                                                                                          cls=JsonBytesEncoder)))
            logger.warning(serializer.errors)


class SubscriptionNextPaymentSerializer(SubscriptionSerializerEventReceiver):
    class Meta:
        events = {
            'NextPayment': NextPaymentEventSerializer
        }
        primary_key_name = 'hash'


class SubscriptionOraclizedDenominationSerializer(SubscriptionSerializerEventReceiver):
    class Meta:
        events = {
            'OraclizedDenomination': OraclizedDenominationEventSerializer
        }
        primary_key_name = 'hash'


class SubscriptionStatusChangedSerializer(SubscriptionSerializerEventReceiver):
    class Meta:
        events = {
            'StatusChanged': StatusChangedEventSerializer
        }
        primary_key_name = 'hash'
